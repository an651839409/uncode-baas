package cn.uncode.baas.server.dto;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class RestApp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8539801603569278105L;

	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String NAME = "name";
	public static final String CREATE_TIME = "createTime";
	public static final String USER = "user";
	public static final String DB_TYPE = "dbType";
	public static final String DB_DRIVER_CLASS = "dbDriverClass";
	public static final String DB_URL = "dbURL";
	public static final String DB_USERNAME = "dbUsername";
	public static final String DB_PASSWORD = "dbPassword";
	public static final String SECRET_KEY = "secretKey";
	public static final String STATUS = "status";

	private int id;
	private String bucket;
	private String name;
	private Date createTime;
	private String user;
	private int dbType;
	private String dbDriverClass;
	private String dbURL;
	private String dbUsername;
	private String dbPassword;
	private String secretKey;
	private int status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getDbType() {
		return dbType;
	}

	public void setDbType(int dbType) {
		this.dbType = dbType;
	}

	public String getDbDriverClass() {
		return dbDriverClass;
	}

	public void setDbDriverClass(String dbDriverClass) {
		this.dbDriverClass = dbDriverClass;
	}

	public String getDbURL() {
		return dbURL;
	}

	public void setDbURL(String dbURL) {
		this.dbURL = dbURL;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getDatabase(){
		if(StringUtils.isNotBlank(dbURL)){
			int end = dbURL.lastIndexOf("?");
			int start = dbURL.lastIndexOf("/")+1;
			if(end == -1){
				end = dbURL.length();
			}
			return dbURL.substring(start, end);
		}
		return null;
	}
	
	

}
