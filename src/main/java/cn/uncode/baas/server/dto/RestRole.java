package cn.uncode.baas.server.dto;

import java.io.Serializable;
import java.util.Map;

public class RestRole  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7677751002702461674L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String NAME = "name";
	public static final String DESC = "desc";
	
	private int id;
	private String bucket;
	private String name;
	private String desc;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public static RestRole valueOf(Map<String, Object> map){
		RestRole restRole = null;
		if(null != map){
			restRole = new RestRole();
			if(map.containsKey(ID)){
				restRole.setId((Integer)map.get(ID));
			}
			if(map.containsKey(BUCKET)){
				restRole.setBucket(String.valueOf(map.get(BUCKET)));
			}
			if(map.containsKey(NAME)){
				restRole.setName(String.valueOf(map.get(NAME)));
			}
			if(map.containsKey(DESC)){
				restRole.setDesc(String.valueOf(map.get(DESC)));
			}
		}
		return restRole;
	}
	
	

}
