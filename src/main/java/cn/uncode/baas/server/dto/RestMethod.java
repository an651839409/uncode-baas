package cn.uncode.baas.server.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import cn.uncode.baas.server.acl.Acl;

public class RestMethod implements Serializable {
	
/**
	 * 
	 */
	private static final long serialVersionUID = -3907094201843465698L;
	
	public static final String ID = "id";
	public static final String BUCKET = "bucket";
	public static final String REST_NAME = "name";
	public static final String OPTION = "option";
	public static final String VERSION = "version";
	public static final String SECONDS = "seconds";
	public static final String SCRIPT = "script";
	public static final String STATUS = "status";
	public static final String ACL = "acl";
	public static final String PARAM_TYPE = "paramType";
	
	private int id;
	
	private String bucket;
	
	private String name;
	
	private String option;
	
	private String version;
	
	private String script;
	
	private int seconds;
	
	private int status;
	
	private String acl;
	
	private int paramType;
	
	private List<RestField> restFields;
	
	public String getKey(){
		return "rester_" + bucket + "_" + name + "_" + option + "_" + version;
	}
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addRestField(RestField restField){
		if(this.restFields == null){
			this.restFields = new ArrayList<RestField>();
		}
		this.restFields.add(restField);
	}

	public List<RestField> getRestFields() {
		return restFields;
	}

	public String getAcl() {
		return acl;
	}

	public void setAcl(String acl) {
		this.acl = acl;
	}

	public int getParamType() {
		return paramType;
	}

	public void setParamType(int paramType) {
		this.paramType = paramType;
	}

	public Acl getRestAcl(){
		if(StringUtils.isNotBlank(acl)){
			Acl ac = new Acl();
			ac.fromJson(acl);
			return ac;
		}
		return null;
	}
	
	
	

}
